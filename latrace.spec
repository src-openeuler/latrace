Name:           latrace
Version:        0.5.11
Release:        16
Summary:        Display library calls using a LD_AUDIT libc feature for glibc 2.4+
License:        GPLv3+
URL:            http://people.redhat.com/jolsa/latrace
Source:         https://people.redhat.com/jolsa/latrace/dl/%{name}-%{version}.tar.bz2

Patch0:         latrace-aarch64-basic-audit.patch

Patch6000:      bugfix-latrace-PRINT-format.patch
Patch6001: 	backport-fix-scanner-configuration.patch

ExclusiveArch:  %{ix86} x86_64 %{arm} aarch64
BuildRequires:  autoconf bison asciidoc xmlto binutils-devel binutils-static gcc

%description
Latrace is able to run a command and display its dynamic library calls using
a LD_AUDIT libc feature (available from libc version 2.4 onward). It is also
capable to measure and display various statistics of dynamic calls.If the
config file is provided, latrace will display symbol's arguments with detailed
output for structures. The config file syntax is similar to the C language, with
several exceptions.The latrace by default fully operates inside of the traced program.
However another pipe mode is available, to move the main work to the latrace binary

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoconf
%configure
make V=1

%install
%make_install ROOTDIR=%{buildroot} V=1
chmod 0755 %{buildroot}/%{_libdir}/libltaudit.so.%{version}

%check
make test

%pre

%preun

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc README
%license COPYING
%config(noreplace)  %{_sysconfdir}/*
%{_bindir}/latrace
%{_bindir}/latrace-ctl
%{_libdir}/libltaudit.so.%{version}

%files help
%defattr(-,root,root)
%doc ReleaseNotes TODO
%{_mandir}/man1/*

%changelog
* Tue Jun 22 2021 shixuantong <shixuantong@huawei.com> - 0.5.11-16
- Type: bugfix
- ID: NA
- SUG: NA
- DESC:add gcc to BuildRequires 

* Wed Sep 9 2020 shixuantong <shixuantong@huawei.com> - 0.5.11-15
- Type: bugfix
- ID: NA
- SUG: NA
- DESC: update Source0

* Wed Aug 5 2020 wenzhanli<wenzhanli2@huawei.com> - 0.5.11-14
- Type:cves
- ID:NA
- SUG:NA
- DESC:Fix make test

* Fri Sep 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.5.11-13
- del patch for ppc64

* Thu Aug 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.5.11-12
- Package init
